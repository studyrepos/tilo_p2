% TiLo 2020 - P2 - Ex1 - List

% append(Xs, Ys, Rs) : Append  list Ys to list Xs and get list Rs
append(nil, Ys, Ys).
append(list(X, Xs), Ys, list(X, Rs)) :- append(Xs, Ys, Rs).

% linlist(Xs) :- Checks if given Xs validates as a list.
% A valid list may be empty or contains one element and a valid list, which may be empty itself.
linlist(nil).
linlist(list(_, Xs)) :- linlist(Xs).

% member(X, Ys) : X is a member of Ys if Ys is a valid list which contains X as head
% or as head of one sublist
member(X, list(X, Ys)) :- linlist(Ys).  % X is the head of the given list
member(X, list(_,Ys)) :-                % X is a member of a sub list
    append(_, list(X,_), Ys),           % Ys may be constructed by appending the lists tail to X
    linlist(Ys).
%   member(X, list(_, Ys)) :- linlist(Ys), member(X, Ys). % alternative as recursive call searching for X as a head

% infix(Xs, Ys) :- Xs is an infix to Ys if Ys contains a prefix prior to Xs and a suffix following Xs
infix(Xs, Ys) :-
    Xs \== nil,                         % there should be no empty list in the middle of another
    append(PreInfix, Suffix, Ys),       % split Ys to preInfixlist and suffixlist
    append(Prefix, Xs, PreInfix),       % split preInfix again, but now with Xs appended to prefix
    PreInfix \== nil,                   % preinfix should not be empty, otherwise the whole list would be empty
    Prefix \== nil,                     % also prefix should not be empty, see preinfix
    Suffix \== nil,                     % suffix should also not be empty, otherwise Xs would be a suffix
    linlist(Xs), linlist(Ys).           % validate lists as linlist

% attach(Xs, X, Ys) :- list Ys is list Xs extended by element X by appending X as list to Ys
attach(Xs, X, Ys) :-
    append(Xs, list(X, nil), Ys),       % append element as a list to Xs
    linlist(Xs), linlist(Ys).           % validate lists as linlist

% rev(Xs, Ys) :- Xs is reverted by reverting its tail and appending its head to this reverted tail
rev(nil, nil). % base case for an empty list which reverts to itself
rev(list(X, Xs), Ys) :-
    rev(Xs, T),                         % reverse tail of list
    attach(T, X, Ys).                   % attach head to reverted tail



% TiLo 2020 - P2 - Ex2 - BinaryTree

% base definition for a node
empty.                                      % empty node
node(_Val, _Ltree, _Rtree).                 % a node as a value with a left and right subtree

% base definition for a binary tree
bintree(empty).                             % an empty tree without nodes or subtrees
bintree(node(_, Ltree, Rtree)) :-           % a binary tree with a node containing a value and two subtrees
    bintree(Ltree), bintree(Rtree).         % the subtrees are binary trees either

% construct
% constructs a new binary tree as a note with given value und two subtrees
construct(Root, Ltree, Rtree, Ntree) :-
    bintree(Ltree), bintree(Rtree),         % validate subtress as binary trees
    Ntree = node(Root, Ltree, Rtree).       % construct a new tree as a node with given value and subtrees

% helper for symbolic numbers
add(o, X, X).
add(s(X), Y, s(XRes)) :- add(X, Y, XRes).

% knotenanz
% counts the number of nodes in a binary tree
knotenanz(empty, o).                        % base, an empty tree with zero nodes
knotenanz(node(_, Ltree, Rtree), N) :-      % binary tree with nodes and subtrees
    knotenanz(Ltree, NL),                   % get number of nodes in left subtree
    knotenanz(Rtree, NR),                   % get number of nodes in right subtree
    add(NL, s(o), T),                       % number of nodes in left subtree plus rootnode
    add(NR, T, N).                          % add number of nodes in right subtree
%   N is NL + NR + 1.
